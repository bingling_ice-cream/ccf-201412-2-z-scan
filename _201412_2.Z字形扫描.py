n=int(input())
a=[]

for i in range(0,n):
    b=list(map(int,input().split()))
    a.append(b)

i=0
j=0

right=1
down=2
rightup=3
leftdown=4

turn=right

while i<n and j<n:
    print(a[i][j],end=" ")

    if turn==right:
        j+=1
        if i==0:
            turn=leftdown
        else:
            turn=rightup
    elif turn==leftdown:
        i+=1
        j-=1
        if i==n-1:
            turn=right
        elif j==0:
            turn=down
    elif turn==down:
        i+=1
        if j==0:
            turn=rightup
        else:
            turn=leftdown
    else:
        i-=1
        j+=1
        if j==n-1:
            turn=down
        elif i==0:
            turn=right